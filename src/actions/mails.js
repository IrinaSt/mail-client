export const DEL_EMAIL = 'DEL_EMAIL';
export const SENT_EMAIL = 'SENT_EMAIL';
export const CHANGE_FOLDER = 'CHANGE_FOLDER';
export const ADD_IMPORTANT = 'ADD_IMPORTANT';
export const OPEN_NEW_LETTER = 'OPEN_NEW_LETTER';
