import React, { Component, Fragment } from 'react';
import store from '../../store';
import { connect } from 'react-redux';
import './menu.scss'

class Menu extends Component {

	state = {
		folders: [
		  {
			
				name: "Входящие",
				id: "getted"
			},
		  {
			
				name: "Исходящие",
				id: "sent"
			}
		]
	}

	changeFolder = (folderName) => {
		store.dispatch({
      type: 'CHANGE_FOLDER',
      payload: folderName
		});
	}

	render() {	
	
	
		let folderList = this.state.folders.map(item => {
			return (<li key={item.id} 
				className={this.props.active ? "active" : null}
				onClick={()=> this.changeFolder(item.id)}
				>
				{item.name}</li>)
				})

		return (
			<Fragment>
				<ul className="main-menu">
				{folderList}
				</ul>
			</Fragment>
		)
	}
}

function mapStateToProps(state){
	return {
	mails: state.mails,
	active: state.active}
};

export default connect(mapStateToProps)(Menu)