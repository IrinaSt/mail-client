import React, { Component, Fragment } from "react";
import {NavLink} from "react-router-dom";
import { connect } from 'react-redux';
import store from '../../store';
import "./mailList.scss";
import Mail from "../Mail";

class MailList extends Component {
  state = {
    openedMsg: [],
  };

  togleMsg = item => {
    let temple = this.state.openedMsg;

    if (temple.includes(item)) {
      temple.splice(temple.indexOf(item), 1);
    } else {
      temple.push(item);
    }
    this.setState({
      openedMsg: temple
    });

    this.setState({
      openedMsg: temple
    });
  };


  addToImportant = (id, active) => {
     store.dispatch({
      type: 'ADD_IMPORTANT',
      payload: id,
      active: active
     })
 }

  getStatus = item => {
    let opnMsg = this.state.openedMsg;
    let status = opnMsg.includes(item) ? null : "hidden";
    return status;
  };

  render() {

    let mailList = this.props.mails.map(item => {
    let isImportant = item.important ? "mail-wrapper active-letter" : 'mail-wrapper'
    let path = '/mail/' + item.id;

      return (
    <div className={isImportant}>
     <span onClick={() => this.addToImportant(item.id, this.props.active)}>&#10052;</span>
    <NavLink to = {path} key={item.id} >
     
      <div onClick={() => this.togleMsg(item.id)} className="mail-item">
        
          {item.subject}
          <p className={this.getStatus(item.id)}>{item.text}</p>
        </div>
       </NavLink>

      </div>
 
      );
    });
    return (
      <Fragment>
        <div className="mail-list">{mailList}</div>
      </Fragment>
    );
  }
}

function mapStateToProps(state){
  return {
    active: state.mails.active,
    mails: state.mails.mailList[state.mails.active],   
  }
};

export default connect(mapStateToProps)(MailList)
