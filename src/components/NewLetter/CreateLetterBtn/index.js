import React, { Component } from 'react'
import './create-letter-btn.scss'

export default class CreateLetterBtn extends Component {
	render() {
		
		return (
		<button className="create-letter-btn" onClick={() => this.props.createLetterControl()}>Create letter</button>
		)
	}
}
