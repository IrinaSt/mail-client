import React, { Component } from "react";
import store from '../../store';
import {connect} from 'react-redux';

class NewLetter extends Component {
  state = {
    emailTo: "",
    subject: "",
    text: ""
	};
	
  changeHandler = event => {
    const name = event.target.name;
    const value = event.target.value;
    this.setState({
      [name]: value
    });
	};
  

  submitHandler = event => {
    event.preventDefault();
    let newLetter = {
      id: Math.random() * (100 - 10) + 10,
      status: true,
      to: this.state.emailTo,
      subject: this.state.subject,
      text: this.state.text
    }
    store.dispatch({
      type: 'SENT_EMAIL',
      payload: newLetter
    });
	};
	
	closeLetter = () =>{
		this.setState({
			emailTo: "",
			subject: "",
			text: ""			
    });
    
     this.props.createLetterControl();
	}


  render() {
    let isOpened = this.props.active ? null : "hidden";
    return (
      <form className={isOpened} action="#" type="POST">
        <input
          type="text"
          name="emailTo"
          value={this.state.emailTo}
          placeholder="To"
          onChange={this.changeHandler}
        />
        <input
          type="text"
          name="subject"
          value={this.state.subject}
          placeholder="Subject"
          onChange={this.changeHandler}
        />
        <textarea name="text" cols="30" rows="10" onChange={this.changeHandler}>
          
        </textarea>
        <button type="submit" onClick={this.submitHandler}>
          Send
        </button>
				<button type="button" onClick={this.closeLetter}>Close</button>
      </form>
    );
  }
}

function mapStateToProps(state){
  return {
    active: state.mails.newLetterOpened,
    mails: state.mails.mailList[state.mails.active],   
  }
};

export default connect(mapStateToProps)(NewLetter)
