import React, { Component, Fragment } from "react";
import store from '../../store';
import {connect} from 'react-redux';
import Header from "../Header";
import Menu from "../Menu";
import MailList from "../MailList";
import NewLetter from "../NewLetter";
import CreateLetterBtn from "../NewLetter/CreateLetterBtn";
import './dashboard.scss'

class Dashboard extends Component {
    state = {
      active: 'getted',
      newLetterOpened: false,
      mailList: {
        getted: [
          {
            id: 1,
            status: true,
            from: 'test@ukr.net',
            subject: 'Some theme',
            text: 'hello my name is Ira',
            isRead: true,
            important: false
          },
          {
            id: 2,
            status: false,
            from: 'test4@ukr.net',
            subject: 'Some theme 6666',
            text: 'hello my name is Yana',
            isRead: true,
            important: false
          },
          {
            id: 3,
            status: true,
            from: 'test1@ukr.net',
            subject: 'Some theme 1111',
            text: 'hello my name is Sasha',
            isRead: true,
            important: false
          }
        ],
        sent: [          {
          id: 4,
          status: true,
          to: 'test666@ukr.net',
          subject: 'Some theme 99',
          text: 'hello my name is Rita',
          important: false
        },
        {
          id: 7,
          status: true,
          to: 'test2@ukr.net',
          subject: 'Some theme 65666',
          text: 'hello my name is Inna',
          important: false
        },
        {
          id: 8,
          status: true,
          to: 'test1111111@ukr.net',
          subject: 'Some theme 2111',
          text: 'hello my name is Anton',
          important: false
        }]
      }
    }
   
    createLetterControl = () =>{
      store.dispatch({
        type: 'OPEN_NEW_LETTER',
        payload: !this.props.newLetterOpened
      });
    }

 
  render() {
   
    return (
     <Fragment>
        <Header />

        <div className="main-content">
        <div className="side-bar">
        <CreateLetterBtn  createLetterControl={this.createLetterControl}/>
        <Menu />
        </div>
        <MailList />
        <NewLetter createLetterControl={this.createLetterControl}/>
       
        
        </div>
    </Fragment>
    );
  }
}

function mapStateToProps(state){
  return {
    newLetterOpened: state.mails.newLetterOpened,
  }
};

export default connect(mapStateToProps)(Dashboard);
