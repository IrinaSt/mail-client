import React, { Component, Fragment } from 'react';
import store from '../../store';
import Menu from '../Menu/';
import Header from '../Header';


export default class Mail extends Component {
  getMails = () => {
		/*     store.getState(); */
let currentState = store.getState();
let activeCategory = currentState.mails.active;
let letters = currentState.mails.mailList[activeCategory];
			 	return letters;
			}
	
	render() {
    let letters =  this.getMails();
		let letter = letters.filter(item =>{
			let newObj ={...item};
			if (newObj.id == this.props.match.params.id) {
				return newObj
			}
	
		})[0];
		console.log(letter)		
		return (
			<Fragment>
				<Header />
				<Menu />
				<p className="letter-from">{letter.from}</p>
				<p className="letter-subject">{letter.subject}</p>
				<p className="letter-text">{letter.text}</p>
			</Fragment>
		)
	}
}
