import React, { Component, Fragment } from 'react'
import {NavLink} from "react-router-dom";
import Search from './Search'
import UserControl from './UserControl'
import './header.scss';


export default class Header extends Component {
	render() {
		return (
			<Fragment>
				
					<div className="main-header">
						<img src="https://ssl.gstatic.com/ui/v1/icons/mail/rfr/logo_gmail_lockup_default_1x.png" alt="logo"/>
					
					<Search />
					<NavLink to='/contacts'>Contacts</NavLink>
					<UserControl />
</div>


			</Fragment>
		)
	}
}
