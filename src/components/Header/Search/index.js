import React, { Component, Fragment } from 'react'
import './search.scss'


export default class Search extends Component {
	render() {
		return (
			<form action="#" type="GET" className="search-form">
				<input type="text" value="" placeholder="Search" className="search-input"/>
				<button className="search-btn" type="submit">Search</button>
			</form>
		)
	}
}
