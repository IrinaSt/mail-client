import { DEL_EMAIL, SENT_EMAIL, CHANGE_FOLDER, ADD_IMPORTANT, OPEN_NEW_LETTER } from "../../actions/mails";

const initialState = {
  active: "getted",
  newLetterOpened: false,
  mailList: {
    getted: [
      {
        id: 1,
        status: true,
        from: "test@ukr.net",
        subject: "Some theme",
        text: "hello my name is Ira",
        isRead: true,
        important: false
      },
      {
        id: 2,
        status: false,
        from: "test4@ukr.net",
        subject: "Some theme 6666",
        text: "hello my name is Yana",
        isRead: true,
        important: false
      },
      {
        id: 3,
        status: true,
        from: "test1@ukr.net",
        subject: "Some theme 1111",
        text: "hello my name is Sasha",
        isRead: true,
        important: false
      }
    ],
    sent: [
      {
        id: 4,
        status: true,
        to: "test666@ukr.net",
        subject: "Some theme 99",
        text: "hello my name is Rita",
        important: false
      },
      {
        id: 7,
        status: true,
        to: "test2@ukr.net",
        subject: "Some theme 65666",
        text: "hello my name is Inna",
        important: false
      },
      {
        id: 8,
        status: true,
        to: "test1111111@ukr.net",
        subject: "Some theme 2111",
        text: "hello my name is Anton",
        important: false
      }
    ]
  }
};

const mails = function(state = initialState, action) {
  switch (action.type) {
    case SENT_EMAIL:
      let updatedSentMailList = [...state.mailList.sent];
      updatedSentMailList.push(action.payload);
      return {
        ...state,
        mailList: {
          ...state.mailList,
          sent: updatedSentMailList
        }
      };

    case DEL_EMAIL:
      return { ...state, mailList: action.payload };

    case CHANGE_FOLDER:
      return { ...state, active: action.payload };

    case ADD_IMPORTANT:
      let mailList = state.mailList[action.active];
      let mailListUpd = state.mailList[action.active].map(item => {
        let newObj = { ...item };
        if (newObj.id == action.payload) {
          newObj.important = !newObj.important;
        }
        return newObj;
      });

      return {
        ...state,
        mailList: {
          ...state.mailList,
          [action.active]: mailListUpd
        }
      };

    case OPEN_NEW_LETTER:
    console.log(state);
    return { ...state, newLetterOpened: action.payload };

    default:
      return state;
  }
};

export default mails;
