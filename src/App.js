import React, {Component} from "react";
import {Route, Switch} from 'react-router-dom'
import Dashboard from './components/Dashboard'
import Mail from './components/Mail'
import Contacts from './components/Contacts'

export default class App extends Component {


  render(){
    return(
    <Switch>
     <Route exact path="/" component={Dashboard} />
     <Route path="/mail/:id" component={Mail} />
     <Route path="/contacts" render = { () => <Contacts />} />
    </Switch>
  )
  }


}
